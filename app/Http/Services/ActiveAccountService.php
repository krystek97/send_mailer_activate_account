<?php

namespace App\Http\Services;

use App\User;
use Carbon\Carbon;

class ActiveAccountService
{
    /**
     * @var User
     */
    private $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function activateAccount($token)
    {
        $message = 'Dziękujemy za potwierdzenie konta';
        $user = $this->user->where(['remember_token' => $token])->first();
        $current_timestamp = Carbon::now()->timestamp;
        $user->email_verified_at = $current_timestamp;
        $user->save();
        $idUser = $user->id;
        return view('users.activateAccount')->with(compact('message', 'idUser'));
    }

    public function accountIsActive($idUser)
    {
        $message = 'Konto już zostało aktywowane';
        return view('users.activateAccount')->with(compact('message', 'idUser'));
    }
}
