<?php

namespace App\Http\Services;

use App\Http\Components\MailComponent;
use App\User;

class RegisterService
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var MailComponent
     */
    private $mailComponent;

    public function __construct(User $user, MailComponent $mailComponent)
    {
        $this->user = $user;
        $this->mailComponent = $mailComponent;
    }

    public function addUserToDatabase($requestData)
    {
        $rememberToken = uniqid(mt_rand(), true);
        $this->user->password = $requestData['password'];
        $this->user->email = $requestData['email'];
        $this->user->remember_token = $rememberToken;
        $this->user->save();
        $this->mailComponent->sendMail($requestData['email'], 'Aktywacja Konta', 'kliknij w link aby aktywować konto <a href="' . url("/activate/account/{$rememberToken}") . '">link</a>');
        return response()->json([
            'type' => 'success',
            'message' => 'Udało się zarejestrować. Wysłano link aktywacyjny na podanego maila',
            'header' => 'Rejestracja'
        ]);
    }
}
