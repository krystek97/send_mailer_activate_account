<?php

namespace App\Http\Services;

use App\Http\Components\MailComponent;
use App\User;

class DeleteAccountService
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var MailComponent
     */
    private $mailComponent;

    public function __construct(User $user, MailComponent $mailComponent)
    {
        $this->user = $user;
        $this->mailComponent = $mailComponent;
    }

    public function deleteUser($idUser)
    {
        $userEmail = $this->user->where(['id' => $idUser])->select('email')->first();
        $this->mailComponent->sendMail($userEmail->email, 'Usunięcie Konta', 'Pomyślnie usunięto konto');
        $this->user->destroy($idUser);
        return response()->json([
            'type' => 'success',
            'message' => 'Udało się usunąć konto',
            'header' => 'Usuwanie konta'
        ]);
    }
}
