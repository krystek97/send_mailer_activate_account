<?php

namespace App\Http\Controllers;

use App\Http\Validators\DeleteAccountValidator;
use Illuminate\Http\Request;

class DeleteAccountController extends Controller
{
    /**
     * @var DeleteAccountValidator
     */
    private $deleteAccountValidator;

    public function __construct(DeleteAccountValidator $deleteAccountValidator)
    {
        $this->deleteAccountValidator = $deleteAccountValidator;
    }

    public function index(Request $request, $id)
    {
        if ($request->isMethod('delete')) {
            return $this->deleteAccountValidator->validateIdToDeleteUser($id);
        }
    }
}
