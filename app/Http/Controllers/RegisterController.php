<?php

namespace App\Http\Controllers;

use App\Http\Services\RegisterService;
use App\Http\Validators\RegisterValidator;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;

class RegisterController extends Controller
{
    /**
     * @var RegisterService
     */
    private $registerService;
    /**
     * @var RegisterValidator
     */
    private $registerValidator;

    public function __construct(RegisterService $registerService, RegisterValidator $registerValidator)
    {
        $this->registerService = $registerService;
        $this->registerValidator = $registerValidator;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('users.add');
        } else if ($request->isMethod('post')) {
            return $this->registerValidator->validateDataToRegisterUser($request->all());
        }
    }
}
