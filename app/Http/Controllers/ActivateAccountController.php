<?php

namespace App\Http\Controllers;

use App\Http\Validators\ActiveAccountValidator;
use Illuminate\Http\Request;

class ActivateAccountController extends Controller
{
    /**
     * @var ActiveAccountValidator
     */
    private $activeAccountValidator;

    public function __construct(ActiveAccountValidator $activeAccountValidator)
    {
        $this->activeAccountValidator = $activeAccountValidator;
    }

    public function index(Request $request, $token)
    {
        if ($request->isMethod('get')) {
            return $this->activeAccountValidator->tokenIsAssignToUser($token);
        }
    }
}
