<?php

namespace App\Http\Validators;

use App\Http\Services\DeleteAccountService;
use App\User;

class DeleteAccountValidator
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var DeleteAccountService
     */
    private $deleteAccountService;

    public function __construct(User $user, DeleteAccountService $deleteAccountService)
    {
        $this->user = $user;
        $this->deleteAccountService = $deleteAccountService;
    }

    public function validateIdToDeleteUser($id)
    {
        $userCount = $this->user->where(['id' => $id])->count();
        if ($userCount === 0) {
            return response()->json([
                'type' => 'error',
                'message' => 'Konto o takim id nie istnieje',
                'header' => 'Usuwanie Konta'
            ]);
        } else {
            return $this->deleteAccountService->deleteUser($id);
        }
    }
}
