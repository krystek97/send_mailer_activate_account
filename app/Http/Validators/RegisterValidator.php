<?php

namespace App\Http\Validators;

use App\Http\Services\RegisterService;
use App\User;
use Illuminate\Support\Facades\Validator;

class RegisterValidator
{
    /**
     * @var RegisterService
     */
    private $registerService;

    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
        $this->user = new User();
    }

    public function validateDataToRegisterUser($requestData)
    {
        $validator = Validator::make($requestData, $this->user->rules);
        if ($validator->fails()) {
            return response()->json([
                'type' => 'error',
                'header' => 'Rejestracja',
                'message' => 'Błędne dane podane przy rejestracji użytkownika'
            ]);
        } else {
            return $this->registerService->addUserToDatabase($requestData);
        }
    }
}
