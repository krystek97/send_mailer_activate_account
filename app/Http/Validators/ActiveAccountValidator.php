<?php

namespace App\Http\Validators;

use App\Http\Services\ActiveAccountService;
use App\User;

class ActiveAccountValidator
{
    /**
     * @var ActiveAccountService
     */
    private $activeAccountService;
    /**
     * @var User
     */
    private $user;

    public function __construct(ActiveAccountService $activeAccountService)
    {
        $this->activeAccountService = $activeAccountService;
        $this->user = new User();
    }

    public function tokenIsAssignToUser($token)
    {
        $countUserWithThisToken = $this->user->where(['remember_token' => $token])->count();
        if ($countUserWithThisToken === 0) {
            return "Podany token nie jest przypisany do żadnego użytkownika";
        } else {
            return $this->accountIsActive($token);
        }
    }

    public function accountIsActive($token)
    {
        $emailVerified = $this->user->where(['remember_token' => $token])->select('email_verified_at', 'id')->first();
        if ($emailVerified->email_verified_at === null) {
            return $this->activeAccountService->activateAccount($token);
        } else {
            return $this->activeAccountService->accountIsActive($emailVerified->id);
        }
    }
}
