<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title')</title>
    <link
        rel="stylesheet"
        href="/css/style.css"
    />
    <link
        rel="stylesheet"
        href="/css/jquery.toasts.css"
    />
    <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous"
    />
    <script
        src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"
    >
    </script>
    <script
        src="/js/jquery.toasts.js"
    >
    </script>
</head>
<body>
    @yield('content')
</body>
</html>


