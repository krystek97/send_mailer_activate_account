@extends('layouts.default')
@section('title')
    Login
@endsection
@section('content')
    <div class="wrapper d-flex justify-content-center align-items-center p-3">
        <div class="loginBox">
            <div class="text-center pt-3">
                <h3>Zarejestruj się</h3>
            </div>
            <form class="form" method="post" id="registerForm" action="/">
                <label>Email</label>
                <input
                    id="email"
                    type="text"
                    name="email"
                    text="Wpisz adres e-mail"
                    additional="required"
                />
                <label>Password</label>
                <input
                    id="password"
                    type="text"
                    name="password"
                    text="Wpisz hasło"
                    additional="required"
                />

                <div class="d-flex justify-content-between align-items-center mt-5">
                    <button type="submit" class="submit">Dalej</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        let emailValue;
        let passwordValue;

        document.getElementById("email").addEventListener("change", (e) => {
            emailValue = e.target.value;
        });
        document.getElementById("password").addEventListener("change", (e) => {
            passwordValue = e.target.value;
        });
        document.querySelector("form").addEventListener("submit", (e) => {
            e.preventDefault();
            console.log(emailValue);
            console.log(passwordValue);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/',
                type: 'post',
                data: $('#registerForm').serialize(),
                cache: false,
                processData: false,
                timeout: 8000,
                dataType: 'json',
                success: function(data) {
                    $.toast({
                        heading: data.header,
                        text: data.message,
                        position: 'top-right',
                        icon: data.type,
                        stack: false
                    })
                    console.log(data);
                }
            });
        });
    </script>
@endsection
