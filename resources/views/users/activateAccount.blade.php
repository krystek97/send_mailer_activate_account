@extends('layouts.default')
@section('title')
    Confirm
@endsection
@section('content')
    <div class="wrapper d-flex justify-content-center align-items-center p-3">
        <div class="loginBox">
            <div class="text-center pt-3">
                <h3>{{$message}}</h3>
            </div>
            <form class="form" method="post">
                <div class="d-flex justify-content-between align-items-center mt-5">
                        <button type="submit" class="delete">Usuń konto</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        document.querySelector("form").addEventListener("submit", (e) => {
            e.preventDefault();
            console.log("deleted");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/delete/account/'+<?php echo $idUser ?>,
                type: 'delete',
                timeout: 8000,
                success: function(data) {
                    $.toast({
                        heading: data.header,
                        text: data.message,
                        position: 'top-right',
                        icon: data.type,
                        stack: false
                    })
                    console.log(data);
                }
            });
        });
    </script>
@endsection
