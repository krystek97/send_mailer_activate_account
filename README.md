## ABOUT PROJECT
Simple project allow register and activate account
## INSTALLATION
From your project root folder to install project run:: <br/>
<code>composer install</code><br/>
From your project root folder to run project run::<br/>
<code>php artisan serve</code><br/>
## DATABASE
Database structure is in the file send_mail_activate_database.sql
## PRODUCTION
https://fierce-wildwood-14144.herokuapp.com/?fbclid=IwAR19FnTBoENN7GNAn0hFLi0h_H6m_GhDBVL3vJSNLqk6daxWRjrWDY2h48A
